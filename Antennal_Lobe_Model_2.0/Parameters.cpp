#pragma warning(disable:4996)
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Parameters.h"
using namespace std;

extern const int mode = 14;
extern const int trials = 50;
extern int cur_brain=0;

//Global Files
//extern FILE * fires = fopen("fires.txt", "w");
extern FILE * fires=NULL;

//timing

double t = 0;
extern const double timestep = .1;
extern const double endtime = 4000.0;

//Network Architecture

extern const int PN_Neurons = 10;
extern const int LN_Neurons = 6;
extern const int number_glomeruli = 6;
//Connection Probabilities

extern const double PN_to_PN_prob = .75;
extern const double PN_to_LN_prob = .75;
extern const double LN_to_PN_prob = .38;
extern const double LN_to_LN_prob = .25;
extern double inter_LN_to_PN_prob = .38;//old value: .55

//Neuron Parameters

extern const double vpn = 14.0 / 3.0;
extern const double vln = -2.0 / 3.0;
extern const double leak = .05;
extern const unsigned int refractory_period = 2;
extern const unsigned int refractory_counts = refractory_period / timestep;
extern const double tau_excitation = 2;
extern const double tau_fast_inhibition = 2;
extern const double tau_slow_inhibition = 750;
extern const double tau_sk = 250; //400
extern const double tau_input = 2;
extern const double PN_to_LN_strength = .006; //lit .0055
extern const double PN_to_PN_strength = .01; //lit .008
extern const double LN_to_LN_fast_strength = .015; //lit .0146
extern const double LN_to_LN_slow_strength = .04; //lit .0106
extern double LN_to_PN_fast_strength = 0.0169;//old .013 lit .0134
extern double LN_to_PN_slow_strength = .0338;//old .026 lit .028
extern double sk_mean = .5;
extern double sk_std_dev = .2;
extern const double input_to_PN_strength = .0040; //lit:.0023
extern const double input_to_LN_strength = .0031; //lit:.0017

//SK Calculation Parameters
extern const double sk_rise = 25; //half rise time
extern const double sk_rise_tau = 5 / sk_rise;

//Odor Parameters
 
extern const double t_rise = 35; //half rise time
extern const double rise_tau = 5 / t_rise;
extern const double background_odor_rate = 3.6; //units of ms^-1 lit:3.8
extern double max_odor_rate = 3.6;
extern const double odor_tau = 384;

//Air Parameters

extern const double air_t_rise = 300; //500 half rise time
extern const double air_rise_tau = 5 / air_t_rise;
extern double max_air_rate = max_odor_rate/2;
extern const double air_tau = 384;


//#pragma warning(disable:4996)
//#include <cstdio>
//#include <cstdlib>
//#include <ctime>
//#include <vector>
//#include <string>
//using namespace std;
//
////Global Files
//extern FILE * fires = fopen("fires.txt", "w");
//
////timing
//
//double t = 0;
//extern const double timestep = .1;
//extern const double endtime = 5000.0;
//
////Network Architecture
//
//extern const int PN_Neurons = 4;
//extern const int LN_Neurons = 6;
//extern const int number_glomeruli = 5;
//
////Connection Probabilities
//
//extern const double PN_to_PN_prob = .75;
//extern const double PN_to_LN_prob = .75;
//extern const double LN_to_PN_prob = .38;
//extern const double LN_to_LN_prob = .25;
//extern const double inter_LN_to_PN_prob = .55;
//
////Neuron Parameters
//
//extern const double vpn = 14 / 3;
//extern const double vln = -2 / 3;
//extern const double leak = .05;
//extern const unsigned int refractory_period = 2;
//extern const unsigned int refractory_counts = refractory_period / timestep;
//extern const double tau_excitation = 2;
//extern const double tau_fast_inhibition = 2;
//extern const double tau_slow_inhibition = 750;
//extern const double tau_sk = 400;
//extern const double tau_input = 2;
//extern const double PN_to_LN_strength = .006; //lit .0055
//extern const double PN_to_PN_strength = .0075;
//extern const double LN_to_LN_fast_strength = .0146;
//extern const double LN_to_LN_slow_strength = .0106; //lit .0106
//extern const double LN_to_PN_fast_strength = 0;//lit .0134
//extern const double LN_to_PN_slow_strength = .023;
//extern const double sk_strength = .5;
//extern const double input_to_PN_strength = .00435; //lit:.0023
//extern const double input_to_LN_strength = .0037; //lit:.0017
//
////Odor Parameters
//
//extern const double t_rise = 35;
//extern const double rise_tau = 5 / t_rise;
//extern const double background_odor_rate = 3.6; //units of ms^-1 lit:3.8
//extern const double max_odor_rate = 3.6;
//extern const double odor_tau = 384;