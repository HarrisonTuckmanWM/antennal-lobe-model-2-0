#pragma once
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Functions.h"
using namespace std;

bool random_check(double prob)
{
	double test = prob * 10000;
	int roll = rand() % 10000;
	if (roll <= test) {
		return true;
	}
	return false;
}
