#pragma once
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "LN_Neuron.h"
#include "PN_Neuron.h"
#include "Functions.h"
using namespace std;

class Glomerulus
{
public:
	Glomerulus();
	~Glomerulus();
	void intraconnect();
	void update();
	void set_PN_signal(double s);
	void set_LN_signal(double s);
	void reset();
	//--------creation-----------------------
	vector<PN_Neuron> PNs;
	vector<LN_Neuron> LNs;

private:
	double signalPN;
	double signalLN;
};
