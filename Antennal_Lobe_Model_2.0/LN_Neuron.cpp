#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Parameters.h"
#include "PN_Neuron.h"
#include "LN_Neuron.h"
using namespace std;


LN_Neuron::LN_Neuron()
{
	//---------------Euler Method Parameters--------------------------------------
	voltage = 0;																			//initialize voltage of Neuron
	refractory_period_internal = 0;																	//initialize refractory period   
	excitation = 0;																			//initialize excitatory current input
	t_excitation = 0;																		//time of most recent excitation input
	fast_inhibition = 0;																	//initialize fast inhibition current input
	t_fast_inhibition = 0;																	//time of most recent fast inhibition input
	slow_inhibition = 0;																	//initialize slow inhibition current input
	t_slow_inhibition = 0;																	//time of most recent slow inhibition input																	//initialize leak current																				//initialize sk inhibition (PN neurons only)
	input = 0;																				//initialize input from stimuli
	t_input = 0;

	//Neural Network Parameters
	PN_connections = {};																			//PN connections from this neuron to neurons in list
	LN_connections = {};																			//Ln connections from this neuron to neurons in list
}

LN_Neuron::~LN_Neuron()
{
}

void LN_Neuron::Integrate() {
	if (refractory_period_internal == 0) {
		double dV = -excitation * exp(-(t - t_excitation) / tau_excitation) / tau_excitation * (voltage - vpn)
			- fast_inhibition * exp(-(t - t_fast_inhibition) / tau_fast_inhibition) / tau_fast_inhibition * (voltage - vln)
			- slow_inhibition * exp(-(t - t_slow_inhibition) / tau_slow_inhibition) / tau_slow_inhibition * (voltage - vln)
			- leak * voltage - input * (voltage - vpn)* exp(-(t - t_input) / tau_input) / tau_input;														//Calculation of dV
		voltage += dV * timestep;															//recalculate voltage
		if (voltage >= 1.0) {																//condition for neuron firing
			voltage = 0;
			refractory_period_internal = refractory_counts;												//refractory period set for 2 ms 
			fire_to_PN();
			fire_to_LN();
			voltage_list.push_back(1);
			fprintf(fires, "1,");
		}
		else {
			voltage_list.push_back(voltage);
			fprintf(fires, "0,");
		}
	}
	else {
		refractory_period_internal -= 1;
		voltage_list.push_back(voltage);
		fprintf(fires, "0,");
	}
	//---------------File Data------------------------------------------
	excitation_list.push_back(excitation * exp(-(t - t_excitation) / tau_excitation)/tau_excitation);
	fast_inhibition_list.push_back(fast_inhibition * exp(-(t - t_fast_inhibition) / tau_fast_inhibition) / tau_fast_inhibition);
	slow_inhibition_list.push_back(slow_inhibition * exp(-(t - t_slow_inhibition) / tau_slow_inhibition) / tau_slow_inhibition);
	input_list.push_back(input);
}


void LN_Neuron::add_PN_connection(PN_Neuron & neuron) {
	PN_connections.push_back(&neuron);
}

void LN_Neuron::add_LN_connection(LN_Neuron & neuron) {
	LN_connections.push_back(&neuron);
}

void LN_Neuron::receiving_excitation(double strength) {
	excitation = excitation * exp(-(t - t_excitation) / tau_excitation) + strength;
	t_excitation = t;
}

void LN_Neuron::receiving_fast_inhibition(double strength) {
	fast_inhibition = fast_inhibition * exp(-(t - t_fast_inhibition) / tau_fast_inhibition) + strength;
	t_fast_inhibition = t;
}

void LN_Neuron::receiving_slow_inhibition(double strength) {
	slow_inhibition = slow_inhibition * exp(-(t - t_slow_inhibition) / tau_slow_inhibition) + strength;
	t_slow_inhibition = t;
}

void LN_Neuron::receiving_input(double strength) {
	input = input * exp(-(t - t_input) / tau_input) + strength;
	t_input = t;
}

void LN_Neuron::fire_to_PN() {
	for (auto i : PN_connections) {
		i->receiving_fast_inhibition(LN_to_PN_fast_strength);
		i->receiving_slow_inhibition(LN_to_PN_slow_strength);
	}
}

void LN_Neuron::fire_to_LN() {
	for (auto i : LN_connections) {
		i->receiving_fast_inhibition(LN_to_LN_fast_strength);
		i->receiving_slow_inhibition(LN_to_LN_slow_strength);
	}
}

double LN_Neuron::get_voltage() {
	return voltage;
}

vector<double> LN_Neuron::get_voltage_list() {
	return voltage_list;
}

vector<double> LN_Neuron::get_excitation_list() {
	return excitation_list;
}

vector<double> LN_Neuron::get_slow_inhibition_list() {
	return slow_inhibition_list;
}

vector<double> LN_Neuron::get_fast_inhibition_list() {
	return fast_inhibition_list;
}

vector<double> LN_Neuron::get_input_list() {
	return input_list;
}

void LN_Neuron::reset() {
	voltage = 0;																			//initialize voltage of Neuron
	refractory_period_internal = 0;															//initialize refractory period   
	excitation = 0;																			//initialize excitatory current input
	t_excitation = 0;																		//time of most recent excitation input
	fast_inhibition = 0;																	//initialize fast inhibition current input
	t_fast_inhibition = 0;																	//time of most recent fast inhibition input
	slow_inhibition = 0;																	//initialize slow inhibition current input
	t_slow_inhibition = 0;																	//time of most recent slow inhibition input	
	input = 0;																				//initialize input from stimuli
	t_input = 0;
	voltage_list.clear();
	excitation_list.clear();
	fast_inhibition_list.clear();
	slow_inhibition_list.clear();
	input_list.clear();
}