#pragma warning(disable:4996)
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include <direct.h>
#include "Parameters.h"
#include "Antennal_Lobe.h"
using namespace std;

int main() {
	srand(123456789);
	if (mode == 1) {
		//fclose(fires);
		//Odor_Pulse odor({ 0,1,2 }, 1000, 1000);
		//Odor_Pulse odor2({ 0,1,2 }, 1143, 50);
		//Odor_Pulse odor3({ 0,1,2 }, 1286, 50);
		//Odor_Pulse odor4({ 0,1,2 }, 1429, 50);
		//Odor_Pulse odor5({ 0,1,2 }, 1571, 50);
		//Odor_Pulse odor6({ 0,1,2 }, 1714, 50);
		//Odor_Pulse odor7({ 0,1,2 }, 1857, 50);
		//Odor_Pulse odor8({ 0,1,2 }, 2000, 50);
		//Odor_Pulse odor9({ 0,1,2 }, 2143, 50);
		//Odor_Pulse odor10({ 0,1,2 }, 2286, 50);
		//Odor_Pulse odor11({ 0,1,2 }, 2429, 50);
		//Odor_Pulse odor12({ 0,1,2 }, 2571, 50);
		//Odor_Pulse odor13({ 0,1,2 }, 2714, 50);
		//Odor_Pulse odor14({ 0,1,2 }, 2857, 50);
		//Odor_Pulse odor15({ 0,1,2 }, 2000, 50);
		//Odor_Pulse odor16({ 0,1,2 }, 2875, 50);
		Air_Puff air(1000, 1000);
		Antennal_Lobe brain;

		//brain.add_odor(odor);
		/*
		brain.add_odor(odor2);
		brain.add_odor(odor3);
		brain.add_odor(odor4);
		brain.add_odor(odor5);
		brain.add_odor(odor6);
		brain.add_odor(odor7);
		brain.add_odor(odor8);
		brain.add_odor(odor9);
		brain.add_odor(odor10);
		brain.add_odor(odor11);
		brain.add_odor(odor12);
		brain.add_odor(odor13);
		brain.add_odor(odor14);
		//brain.add_odor(odor15);
		//brain.add_odor(odor16);
		*/
		brain.add_air(air);


		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
			t = 0;
			printf("%d\n", cur_brain);
			string str = "fires" + to_string(cur_brain) + ".txt";
			fires = fopen(str.c_str(), "w");
			while (t < endtime) {
				brain.update();
				fprintf(fires, "\n");
				t += timestep;
			}
			fclose(fires);
			if (cur_brain == 1) {
				brain.write_times("./");
			}
			brain.write_all("./");
			brain.reset();
		}
	}
	else if (mode == 2) {
		vector<string> Odor_String = { "./0,1,2","./0,1,3","./0,1,4","./0,1,5","./0,2,3","./0,2,4","./0,2,5","./0,3,4","./0,3,5","./0,4,5","./1,2,3","./1,2,4","./1,2,5","./1,3,4","./1,3,5","./1,4,5","./2,3,4","./2,3,5","./2,4,5","./3,4,5" };
		int u = 0;
		int v = 1;
		int w = 2;
		for (size_t i = 0; i < Odor_String.size(); i++)
		{
			mkdir(Odor_String[i].c_str());
			srand(123456789);
			Odor_Pulse odor({ u,v,w }, 1000, 1000);
			w += 1;
			if (w > 5) {
				v += 1;
				if (v > 4) {
					u += 1;
					v = u + 1;
				}
				w = v + 1;
			}
			Air_Puff air(1000, 1000);
			Antennal_Lobe brain;

			brain.add_odor(odor);
			//brain.add_air(air);

			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = Odor_String[i] + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = Odor_String[i] + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all();
				brain.reset();
			}
		}
	}
	else if (mode == 3) {
		LN_to_PN_slow_strength = 0;
		vector<string> Odor_String = { "./000slow","./005slow","./010slow","./015slow","./020slow","./025slow","./030slow","./035slow","./040slow","./045slow","./050slow","./055slow","./060slow","./065slow","./070slow","./075slow","./080slow","./085slow","./090slow","./095slow","./100slow" };
		for (size_t i = 0; i < Odor_String.size(); i++)
		{
			mkdir(Odor_String[i].c_str());
			srand(123456789);
			Odor_Pulse odor({ 0,1,2 }, 1000, 1000);
			Air_Puff air(1000, 1000);
			Antennal_Lobe brain;

			brain.add_odor(odor);
			//brain.add_air(air);

			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = Odor_String[i] + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = Odor_String[i] + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all();
				brain.reset();
			}
			LN_to_PN_slow_strength += .005;
		}
	}
	else if (mode == 4) {
		inter_LN_to_PN_prob = 0;
		vector<string> Odor_String = { "./0prob","./1prob","./2prob","./3prob","./4prob","./5prob","./6prob","./7prob","./8prob","./9prob","./10prob" };
		for (size_t i = 0; i < Odor_String.size(); i++)
		{
			mkdir(Odor_String[i].c_str());
			srand(123456789);
			Odor_Pulse odor({ 0,1,2 }, 1000, 1000);
			Air_Puff air(1000, 1000);
			Antennal_Lobe brain;

			//brain.add_odor(odor);
			brain.add_air(air);

			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = Odor_String[i] + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = Odor_String[i] + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all();
				brain.reset();
			}
			inter_LN_to_PN_prob += .1;
		}
	}
	else if (mode == 5) {
		vector<string> Odor_String = { "./2Hz","./3Hz","./4Hz","./5Hz","./6Hz","./7Hz","./8Hz" };
		vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		Antennal_Lobe brain;
		for (size_t i = 0; i < Odor_String.size(); i++)
		{
			srand(123456789);
			mkdir(Odor_String[i].c_str());
			//vector<Odor_Pulse> Odor_Vec = {};
			vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				//Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
				Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				//brain.add_odor(Odor_Vec[j]);
				brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = Odor_String[i] + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = Odor_String[i] + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				brain.write_all(Odor_String[i]);
				brain.reset();
			}
			brain.hard_reset();
		}
	}
	else if (mode == 6) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		LN_to_PN_slow_strength = 0;
		vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
		//		vector<Odor_Pulse> Odor_Vec = {};
		//		//vector<Air_Puff> Air_Vec = {};
		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
		//			//Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
		//		}

		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			brain.add_odor(Odor_Vec[j]);
		//			//brain.add_air(Air_Vec[j]);
		//		}


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_slow_strength += .005;
		//}
		//ScenarioString = "./Mechano";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_slow_strength = 0;
		////vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		////vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		////vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		////Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
		//		//vector<Odor_Pulse> Odor_Vec = {};
		//		vector<Air_Puff> Air_Vec = {};
		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			//Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
		//			Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
		//		}

		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			//brain.add_odor(Odor_Vec[j]);
		//			brain.add_air(Air_Vec[j]);
		//		}


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_slow_strength += .005;
		//}
		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());

		LN_to_PN_slow_strength = 0;
		//vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		//vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		//vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		//Antennal_Lobe brain;
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
				vector<Odor_Pulse> Odor_Vec = {};
				vector<Air_Puff> Air_Vec = {};
				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
					Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
				}

				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					brain.add_odor(Odor_Vec[j]);
					brain.add_air(Air_Vec[j]);
				}


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			LN_to_PN_slow_strength += .005;
		}
		//ScenarioString = "./Normalized";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_slow_strength = 0;
		//max_odor_rate = max_odor_rate / 2;
		//max_air_rate = max_air_rate / 2;
		////vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		////vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		////vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		////Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
		//		vector<Odor_Pulse> Odor_Vec = {};
		//		vector<Air_Puff> Air_Vec = {};
		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
		//			Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
		//		}

		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			brain.add_odor(Odor_Vec[j]);
		//			brain.add_air(Air_Vec[j]);
		//		}


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_slow_strength += .005;
		//}
	}
	else if (mode == 7) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		LN_to_PN_fast_strength = 0;
		vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
		//		vector<Odor_Pulse> Odor_Vec = {};
		//		//vector<Air_Puff> Air_Vec = {};
		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
		//			//Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
		//		}

		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			brain.add_odor(Odor_Vec[j]);
		//			//brain.add_air(Air_Vec[j]);
		//		}


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_fast_strength += .005;
		//}
		//ScenarioString = "./Mechano";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_fast_strength = 0;
		////vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		////vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		////vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		////Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
		//		//vector<Odor_Pulse> Odor_Vec = {};
		//		vector<Air_Puff> Air_Vec = {};
		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			//Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
		//			Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
		//		}

		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			//brain.add_odor(Odor_Vec[j]);
		//			brain.add_air(Air_Vec[j]);
		//		}


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_fast_strength += .005;
		//}
		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());

		LN_to_PN_fast_strength = 0;
		//vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		//vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		//vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		//Antennal_Lobe brain;
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
				vector<Odor_Pulse> Odor_Vec = {};
				vector<Air_Puff> Air_Vec = {};
				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
					Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
				}

				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					brain.add_odor(Odor_Vec[j]);
					brain.add_air(Air_Vec[j]);
				}


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			LN_to_PN_fast_strength += .005;
		}
		//ScenarioString = "./Normalized";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_fast_strength = 0;
		//max_odor_rate = max_odor_rate / 2;
		//max_air_rate = max_air_rate / 2;
		////vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		////vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		////vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		////Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
		//		vector<Odor_Pulse> Odor_Vec = {};
		//		vector<Air_Puff> Air_Vec = {};
		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
		//			Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
		//		}

		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			brain.add_odor(Odor_Vec[j]);
		//			brain.add_air(Air_Vec[j]);
		//		}


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_fast_strength += .005;
		//}
	}
	else if (mode == 8) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		sk_mean = 0;
		sk_std_dev = 0;
		vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			Antennal_Lobe brain;
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
				vector<Odor_Pulse> Odor_Vec = {};
				//vector<Air_Puff> Air_Vec = {};
				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
					//Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
				}

				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					brain.add_odor(Odor_Vec[j]);
					//brain.add_air(Air_Vec[j]);
				}


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			sk_mean += .1;
		}
		ScenarioString = "./Mechano";
		mkdir(ScenarioString.c_str());

		sk_mean = 0;
		sk_std_dev = 0;
		//vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		//vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		//vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			Antennal_Lobe brain;
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
				//vector<Odor_Pulse> Odor_Vec = {};
				vector<Air_Puff> Air_Vec = {};
				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					//Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
					Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
				}

				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					//brain.add_odor(Odor_Vec[j]);
					brain.add_air(Air_Vec[j]);
				}


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			sk_mean += .1;
		}
		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());

		sk_mean = 0;
		sk_std_dev = 0;
		//vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		//vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		//vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			Antennal_Lobe brain;
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
				vector<Odor_Pulse> Odor_Vec = {};
				vector<Air_Puff> Air_Vec = {};
				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
					Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
				}

				for (size_t j = 0; j < Frequency[i].size(); j++)
				{
					brain.add_odor(Odor_Vec[j]);
					brain.add_air(Air_Vec[j]);
				}


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			sk_mean += .1;
		}
		//ScenarioString = "./Normalized";
		//mkdir(ScenarioString.c_str());

		//sk_mean = 0;
		//sk_std_dev = 0;
		//max_odor_rate = max_odor_rate / 2;
		//max_air_rate = max_air_rate / 2;
		////vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		////vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		////vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	Antennal_Lobe brain;
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());
		//		vector<Odor_Pulse> Odor_Vec = {};
		//		vector<Air_Puff> Air_Vec = {};
		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
		//			Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
		//		}

		//		for (size_t j = 0; j < Frequency[i].size(); j++)
		//		{
		//			brain.add_odor(Odor_Vec[j]);
		//			brain.add_air(Air_Vec[j]);
		//		}


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	sk_mean += .1;
		//}
	}
	else if (mode == 9) {
		string ScenarioString = "./Odor";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_fast_strength = 0;
		vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

		//		Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
		//		//Air_Puff air(1000, i * 50 + 50);
		//		brain.add_odor(odor);
		//		//brain.add_air(air);


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_fast_strength += .005;
		//}
		//ScenarioString = "./Mechano";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_fast_strength = 0;
		////vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		////vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		////Antennal_Lobe brain;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

		//		//Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
		//		Air_Puff air(1000, i * 50 + 50);
		//		//brain.add_odor(odor);
		//		brain.add_air(air);


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_fast_strength += .005;
		//}
		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());

		LN_to_PN_fast_strength = 0;
		//vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		//vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		//Antennal_Lobe brain;
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

				Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
				Air_Puff air(1000, i * 50 + 50);
				brain.add_odor(odor);
				brain.add_air(air);


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			LN_to_PN_fast_strength += .005;
		}
		//ScenarioString = "./Normalized";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_fast_strength = 0;
		////vector<string> Odor_String = { "/000fast","/005fast","/010fast","/015fast","/020fast","/025fast","/030fast","/035fast","/040fast","/045fast","/050fast","/055fast","/060fast","/065fast","/070fast","/075fast","/080fast","/085fast","/090fast","/095fast","/100fast" };
		////vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		////Antennal_Lobe brain;
		//max_odor_rate = max_odor_rate / 2;
		//max_air_rate = max_air_rate / 2;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

		//		Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
		//		Air_Puff air(1000, i * 50 + 50);
		//		brain.add_odor(odor);
		//		brain.add_air(air);


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_fast_strength += .005;
		//}
	}
	else if (mode == 10) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		LN_to_PN_slow_strength = 0;
		vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		Antennal_Lobe brain;
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

				Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
				//Air_Puff air(1000, i * 50 + 50);
				brain.add_odor(odor);
				//brain.add_air(air);


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			LN_to_PN_slow_strength += .005;
		}
		ScenarioString = "./Mechano";
		mkdir(ScenarioString.c_str());

		LN_to_PN_slow_strength = 0;
		//vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		//vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		//Antennal_Lobe brain;
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

				//Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
				Air_Puff air(1000, i * 50 + 50);
				//brain.add_odor(odor);
				brain.add_air(air);


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			LN_to_PN_slow_strength += .005;
		}
		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());

		LN_to_PN_slow_strength = 0;
		//vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		//vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		//Antennal_Lobe brain;
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

				Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
				Air_Puff air(1000, i * 50 + 50);
				brain.add_odor(odor);
				brain.add_air(air);


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			LN_to_PN_slow_strength += .005;
		}
		//ScenarioString = "./Normalized";
		//mkdir(ScenarioString.c_str());

		//LN_to_PN_slow_strength = 0;
		////vector<string> Odor_String = { "/000slow","/005slow","/010slow","/015slow","/020slow","/025slow","/030slow","/035slow","/040slow","/045slow","/050slow","/055slow","/060slow","/065slow","/070slow","/075slow","/080slow","/085slow","/090slow","/095slow","/100slow" };
		////vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		////Antennal_Lobe brain;
		//max_odor_rate = max_odor_rate / 2;
		//max_air_rate = max_air_rate / 2;
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

		//		Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
		//		Air_Puff air(1000, i * 50 + 50);
		//		brain.add_odor(odor);
		//		brain.add_air(air);


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	LN_to_PN_slow_strength += .005;
		//}
	}
	else if (mode == 11) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		sk_mean = 0;
		sk_std_dev = 0;
		vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		//vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			Antennal_Lobe brain;
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

				Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
				//Air_Puff air(1000, i * 50 + 50);
				brain.add_odor(odor);
				//brain.add_air(air);


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			sk_mean += .1;
		}
		ScenarioString = "./Mechano";
		mkdir(ScenarioString.c_str());

		sk_mean = 0;
		sk_std_dev = 0;
		//vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		//vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		//vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			Antennal_Lobe brain;
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

				//Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
				Air_Puff air(1000, i * 50 + 50);
				//brain.add_odor(odor);
				brain.add_air(air);


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			sk_mean += .1;
		}
		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());

		sk_mean = 0;
		sk_std_dev = 0;
		//vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		//vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		//vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		for (size_t k = 0; k < Odor_String.size(); k++)
		{
			Antennal_Lobe brain;
			mkdir((ScenarioString + Odor_String[k]).c_str());
			for (size_t i = 0; i < Odor_String2.size(); i++)
			{
				srand(123456789);
				mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

				Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
				Air_Puff air(1000, i * 50 + 50);
				brain.add_odor(odor);
				brain.add_air(air);


				for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
					t = 0;
					printf("%d\n", cur_brain);
					string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
					fires = fopen(str.c_str(), "w");
					while (t < endtime) {
						brain.update();
						fprintf(fires, "\n");
						t += timestep;
					}
					fclose(fires);
					if (cur_brain == 1) {
						string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
						FILE* file = fopen(time_str.c_str(), "w");
						for (auto i : brain.times) {
							fprintf(file, "%f\n", i);
						}
						fclose(file);
					}
					//brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
					brain.reset();
				}
				brain.hard_reset();
			}
			sk_mean += .1;
		}
		//ScenarioString = "./Normalized";
		//mkdir(ScenarioString.c_str());

		//sk_mean = 0;
		//sk_std_dev = 0;
		//max_odor_rate = max_odor_rate / 2;
		//max_air_rate = max_air_rate / 2;
		////vector<string> Odor_String = { "/0sk","/1sk","/2sk","/3sk","/4sk","/5sk" ,"/6sk" ,"/7sk" ,"/8sk" ,"/9sk" ,"/10sk" };
		////vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		////vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		//for (size_t k = 0; k < Odor_String.size(); k++)
		//{
		//	Antennal_Lobe brain;
		//	mkdir((ScenarioString + Odor_String[k]).c_str());
		//	for (size_t i = 0; i < Odor_String2.size(); i++)
		//	{
		//		srand(123456789);
		//		mkdir((ScenarioString + Odor_String[k] + Odor_String2[i]).c_str());

		//		Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
		//		Air_Puff air(1000, i * 50 + 50);
		//		brain.add_odor(odor);
		//		brain.add_air(air);


		//		for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
		//			t = 0;
		//			printf("%d\n", cur_brain);
		//			string str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
		//			fires = fopen(str.c_str(), "w");
		//			while (t < endtime) {
		//				brain.update();
		//				fprintf(fires, "\n");
		//				t += timestep;
		//			}
		//			fclose(fires);
		//			if (cur_brain == 1) {
		//				string time_str = (ScenarioString + Odor_String[k] + Odor_String2[i]) + "/time.txt";
		//				FILE* file = fopen(time_str.c_str(), "w");
		//				for (auto i : brain.times) {
		//					fprintf(file, "%f\n", i);
		//				}
		//				fclose(file);
		//			}
		//			brain.write_all((ScenarioString + Odor_String[k] + Odor_String2[i]));
		//			brain.reset();
		//		}
		//		brain.hard_reset();
		//	}
		//	sk_mean += .1;
		//}
	}
	else if (mode == 12) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		vector<string> Odor_String2 = { "/50ms","/100ms","/150ms","/200ms","/250ms","/300ms","/350ms","/400ms","/450ms","/500ms" };
		Antennal_Lobe brain;
		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());

			Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
			//Air_Puff air(1000, i * 50 + 50);
			brain.add_odor(odor);
			//brain.add_air(air);


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}

		ScenarioString = "./Mechano";
		mkdir(ScenarioString.c_str());

		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());

			//Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
			Air_Puff air(1000, i * 50 + 50);
			//brain.add_odor(odor);
			brain.add_air(air);


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}

		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());

		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());

			Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
			Air_Puff air(1000, i * 50 + 50);
			brain.add_odor(odor);
			brain.add_air(air);


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}

		/*ScenarioString = "./Normalized";
		mkdir(ScenarioString.c_str());


		max_odor_rate = max_odor_rate / 2;
		max_air_rate = max_air_rate / 2;

		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());

			Odor_Pulse odor({ 0,1,2 }, 1000, i * 50 + 50);
			Air_Puff air(1000, i * 50 + 50);
			brain.add_odor(odor);
			brain.add_air(air);


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}*/
	}
	else if (mode == 13) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		Antennal_Lobe brain;
		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			vector<Odor_Pulse> Odor_Vec = {};
			//vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
				//Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				brain.add_odor(Odor_Vec[j]);
				//brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}
		ScenarioString = "./Mechano";
		mkdir(ScenarioString.c_str());

		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			//vector<Odor_Pulse> Odor_Vec = {};
			vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				//Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
				Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				//brain.add_odor(Odor_Vec[j]);
				brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}

		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());


		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			vector<Odor_Pulse> Odor_Vec = {};
			vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
				Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				brain.add_odor(Odor_Vec[j]);
				brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}

		/*ScenarioString = "./Normalized";
		mkdir(ScenarioString.c_str());

		max_odor_rate = max_odor_rate / 2;
		max_air_rate = max_air_rate / 2;

		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			vector<Odor_Pulse> Odor_Vec = {};
			vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
				Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				brain.add_odor(Odor_Vec[j]);
				brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();*/
		//}

	}
	else if (mode == 14) {
		string ScenarioString = "./Odor";
		mkdir(ScenarioString.c_str());

		vector<string> Odor_String2 = { "/2Hz","/3Hz","/4Hz","/5Hz","/6Hz","/7Hz","/8Hz" };
		vector<vector<int>> Frequency = { {1000,1500,2000,2500},{1000,1333,1667,2000,2333,2667},{1000,1250,1500,1750,2000,2250,2500,2750},{1000,1200,1400,1600,1800,2000,2200,2400,2600,2800},{1000,1167,1333,1500,1667,1833,2000,2167,2333,2500,2667,2833},{1000,1142,1286,1429,1571,1714,1857,2000,2142,2286,2429,2571,2714,2857},{1000,1125,1250,1375,1500,1625,1750,1875,2000,2125,2250,2375,2500,2625,2750,2875} };
		Antennal_Lobe brain;
		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			vector<Odor_Pulse> Odor_Vec = {};
			//vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				Odor_Vec.push_back(Odor_Pulse({ 0,1,2,3,4,5 }, Frequency[i][j], 50));
				//Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				brain.add_odor(Odor_Vec[j]);
				//brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}
		ScenarioString = "./Mechano";
		mkdir(ScenarioString.c_str());

		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			//vector<Odor_Pulse> Odor_Vec = {};
			vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				//Odor_Vec.push_back(Odor_Pulse({ 0,1,2 }, Frequency[i][j], 50));
				Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				//brain.add_odor(Odor_Vec[j]);
				brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}

		ScenarioString = "./Additive";
		mkdir(ScenarioString.c_str());


		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			vector<Odor_Pulse> Odor_Vec = {};
			vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				Odor_Vec.push_back(Odor_Pulse({ 0,1,2,3,4,5 }, Frequency[i][j], 50));
				Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				brain.add_odor(Odor_Vec[j]);
				brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				//brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}

		/*ScenarioString = "./Normalized";
		mkdir(ScenarioString.c_str());

		max_odor_rate = max_odor_rate / 2;
		max_air_rate = max_air_rate / 2;

		for (size_t i = 0; i < Odor_String2.size(); i++)
		{
			srand(123456789);
			mkdir((ScenarioString + Odor_String2[i]).c_str());
			vector<Odor_Pulse> Odor_Vec = {};
			vector<Air_Puff> Air_Vec = {};
			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				Odor_Vec.push_back(Odor_Pulse({ 0,1,2,3,4,5 }, Frequency[i][j], 50));
				Air_Vec.push_back(Air_Puff(Frequency[i][j], 50));
			}

			for (size_t j = 0; j < Frequency[i].size(); j++)
			{
				brain.add_odor(Odor_Vec[j]);
				brain.add_air(Air_Vec[j]);
			}


			for (cur_brain = 1; cur_brain < trials + 1; cur_brain++) {
				t = 0;
				printf("%d\n", cur_brain);
				string str = (ScenarioString + Odor_String2[i]) + "/fires" + to_string(cur_brain) + ".txt";
				fires = fopen(str.c_str(), "w");
				while (t < endtime) {
					brain.update();
					fprintf(fires, "\n");
					t += timestep;
				}
				fclose(fires);
				if (cur_brain == 1) {
					string time_str = (ScenarioString + Odor_String2[i]) + "/time.txt";
					FILE* file = fopen(time_str.c_str(), "w");
					for (auto i : brain.times) {
						fprintf(file, "%f\n", i);
					}
					fclose(file);
				}
				brain.write_all((ScenarioString + Odor_String2[i]));
				brain.reset();
			}
			brain.hard_reset();
		}*/
	}
	return 0;
}