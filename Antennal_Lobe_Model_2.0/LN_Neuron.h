#pragma once
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "PN_Neuron.h"
using namespace std;

class LN_Neuron
{
public:
	LN_Neuron();
	~LN_Neuron();
	void add_PN_connection(PN_Neuron & neuron);
	void add_LN_connection(LN_Neuron & neuron);
	void Integrate();																		// Use Euler Method to calculate voltage
	void fire_to_PN();																		//send signal to PN connections
	void fire_to_LN();																		//send signal to LN connections
	void receiving_excitation(double strength);												//excitation signal received from other neurons
	void receiving_fast_inhibition(double strength);										//fast inhibition signal received from other neurons
	void receiving_slow_inhibition(double strength);										//slow inhibition signal received from other neurons 
	void receiving_input(double strength);
	double get_voltage();
	vector<double> get_voltage_list();
	vector<double> get_excitation_list();
	vector<double> get_slow_inhibition_list();
	vector<double> get_fast_inhibition_list();
	vector<double> get_input_list();
	void reset();
private:
	double voltage;
	unsigned int refractory_period_internal;
	double excitation;
	double t_excitation;																//decay rate of excitation signal
	double fast_inhibition;
	double t_fast_inhibition;														//decay rate of fast inhibition signal
	double slow_inhibition;
	double t_slow_inhibition;															//decay rate of slow inhibition signal
	double input;
	double t_input;
	vector<PN_Neuron*> PN_connections;
	vector<LN_Neuron*> LN_connections;
	vector<double> voltage_list;
	vector<double> excitation_list;
	vector<double> fast_inhibition_list;
	vector<double> slow_inhibition_list;
	vector<double> input_list;
};

