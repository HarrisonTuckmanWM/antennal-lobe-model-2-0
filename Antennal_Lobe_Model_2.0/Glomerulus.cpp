#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Glomerulus.h"
#include "Parameters.h"
using namespace std;



Glomerulus::Glomerulus()
{
	signalPN = background_odor_rate;
	signalLN = background_odor_rate;
	//--------creating neurons---------------
	PNs.resize(PN_Neurons);
	LNs.resize(LN_Neurons);
	//--------connecting neurons-------------
	intraconnect();
}

Glomerulus::~Glomerulus()
{
}

void Glomerulus::intraconnect() {
	for (size_t i = 0; i < size(PNs); i++)
	{
		for (size_t j = 0; j < size(PNs); j++)
		{
			if (i != j) {
				if (random_check(PN_to_PN_prob)) {
					PNs[i].add_PN_connection(PNs[j]);
				}
			}
		}
		for (size_t j = 0; j < size(LNs); j++)
		{
			if (random_check(PN_to_LN_prob)) {
				PNs[i].add_LN_connection(LNs[j]);
			}
		}
	}
	for (size_t i = 0; i < size(LNs); i++)
	{
		for (size_t j = 0; j < size(PNs); j++)
		{
			if (random_check(LN_to_PN_prob)) {
				LNs[i].add_PN_connection(PNs[j]);
			}
		}
		for (size_t j = 0; j < size(LNs); j++)
		{
			if (i != j) {
				if (random_check(LN_to_LN_prob)) {
					LNs[i].add_LN_connection(LNs[j]);
				}
			}
		}
	}
}

void Glomerulus::update() {
	for (size_t i = 0; i < size(PNs); i++)
	{
		if (random_check(signalPN*timestep)) {
			PNs[i].receiving_input(input_to_PN_strength);
		}
	}
	for (size_t i = 0; i < size(LNs); i++)
	{
		if (random_check(signalLN*timestep)) {
			LNs[i].receiving_input(input_to_LN_strength);
		}
	}
	for (size_t i = 0; i < size(PNs); i++)
	{
		PNs[i].Integrate();
	}
	for (size_t i = 0; i < size(LNs); i++)
	{
		LNs[i].Integrate();
	}
	signalPN = background_odor_rate;
	signalLN = background_odor_rate;
}

void Glomerulus::set_PN_signal(double s) {
	signalPN += s;
}

void Glomerulus::set_LN_signal(double s) {
	signalLN += s;
}

void Glomerulus::reset() {
	for (size_t i = 0; i < size(PNs); i++)
	{
		PNs[i].reset();
	}
	for (size_t i = 0; i < size(LNs); i++)
	{
		LNs[i].reset();
	}
}