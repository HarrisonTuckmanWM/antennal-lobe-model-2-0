#pragma warning (disable : 4996)
#include "Antennal_Lobe.h"





Antennal_Lobe::Antennal_Lobe()
{
	glomeruli.resize(number_glomeruli);
	odors = {};
	interconnect();
}

Antennal_Lobe::~Antennal_Lobe()
{
}

void Antennal_Lobe::interconnect() {
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		for (size_t j = 0; j < size(glomeruli); j++)
		{
			for (size_t k = 0; k < size(glomeruli[i].LNs); k++)
			{
				for (size_t l = 0; l < size(glomeruli[j].PNs); l++)
				{
					if (random_check(inter_LN_to_PN_prob)) {
						glomeruli[i].LNs[k].add_PN_connection(glomeruli[j].PNs[l]);
					}
				}
			}
		}
	}
}

void Antennal_Lobe::update() {
	for (auto odor : odors) {
		odor->update();
		if (mode == 14) {
			for (int glom : odor->get_glomeruli()) {
				glomeruli[glom].set_PN_signal(glom * .2 * (odor->get_PN_signal()));
				glomeruli[glom].set_LN_signal(glom * .2 * (odor->get_LN_signal()));
			}
		}
		else {
			for (int glom : odor->get_glomeruli()) {
				glomeruli[glom].set_PN_signal(odor->get_PN_signal());
				glomeruli[glom].set_LN_signal(odor->get_LN_signal());
			}
		}
	}
	for (auto air : airs) {
		air->update();
		for (int glom = 0; glom < number_glomeruli; glom++) {
			glomeruli[glom].set_PN_signal(air->get_PN_signal());
			glomeruli[glom].set_LN_signal(air->get_LN_signal());
		}
	}
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		glomeruli[i].update();
	}
	times.push_back(t);
}

void Antennal_Lobe::write_times(string dir) {
	string str = dir + "/time.txt";
	FILE* file = fopen(str.c_str(), "w");
	for (auto i : times) {
		fprintf(file, "%f\n", i);
	}
	fclose(file);
}

void Antennal_Lobe::add_odor(Odor_Pulse& odor) {
	odors.push_back(&odor);
}

void Antennal_Lobe::clear_odors() {
	odors = {};
}

void Antennal_Lobe::add_air(Air_Puff& air) {
	airs.push_back(&air);
}

void Antennal_Lobe::clear_airs() {
	airs = {};
}

void Antennal_Lobe::write_voltage(string dir) {
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		for (size_t j = 0; j < size(glomeruli[i].PNs); j++)
		{
			string str = dir + "/pn_voltage_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].PNs[j].get_voltage_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
		for (size_t j = 0; j < size(glomeruli[i].LNs); j++)
		{
			string str = dir + "/ln_voltage_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].LNs[j].get_voltage_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
	}
}

void Antennal_Lobe::write_excitation(string dir) {
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		for (size_t j = 0; j < size(glomeruli[i].PNs); j++)
		{
			string str = dir + "/pn_excitation_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].PNs[j].get_excitation_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
		for (size_t j = 0; j < size(glomeruli[i].LNs); j++)
		{
			string str = dir + "/ln_excitation_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].LNs[j].get_excitation_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
	}
}

void Antennal_Lobe::write_fast_inhibition(string dir) {
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		for (size_t j = 0; j < size(glomeruli[i].PNs); j++)
		{
			string str = dir + "/pn_fast_inhibition_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].PNs[j].get_fast_inhibition_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
		for (size_t j = 0; j < size(glomeruli[i].LNs); j++)
		{
			string str = dir + "/ln_fast_inhibition_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].LNs[j].get_fast_inhibition_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
	}
}

void Antennal_Lobe::write_slow_inhibition(string dir) {
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		for (size_t j = 0; j < size(glomeruli[i].PNs); j++)
		{
			string str = dir + "/pn_slow_inhibition_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].PNs[j].get_slow_inhibition_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
		for (size_t j = 0; j < size(glomeruli[i].LNs); j++)
		{
			string str = dir + "/ln_slow_inhibition_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].LNs[j].get_slow_inhibition_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
	}
}

void Antennal_Lobe::write_input(string dir) {
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		for (size_t j = 0; j < size(glomeruli[i].PNs); j++)
		{
			string str = dir + "/pn_input_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].PNs[j].get_input_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
		for (size_t j = 0; j < size(glomeruli[i].LNs); j++)
		{
			string str = dir + "/ln_input_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].LNs[j].get_input_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
	}
}

void Antennal_Lobe::write_sk(string dir) {
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		for (size_t j = 0; j < size(glomeruli[i].PNs); j++)
		{
			string str = dir + "/pn_sk_glomerulus_" + to_string(i) + "_neuron_" + to_string(j) + "_run_" + to_string(cur_brain) + ".txt";
			FILE* file = fopen(str.c_str(), "w");
			for (auto k : glomeruli[i].PNs[j].get_sk_list()) {
				fprintf(file, "%f\n", k);
			}
			fclose(file);
		}
	}
}

void Antennal_Lobe::write_all(string dir) {
	write_voltage(dir);
	write_excitation(dir);
	write_slow_inhibition(dir);
	write_fast_inhibition(dir);
	write_sk(dir);
	write_input(dir);
}

void Antennal_Lobe::reset() {
	times.clear();
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		glomeruli[i].reset();
	}
	for (auto odor : odors) {
		odor->reset();
	}
	for (auto air : airs) {
		air->reset();
	}
}

void Antennal_Lobe::hard_reset() {
	times.clear();
	for (size_t i = 0; i < size(glomeruli); i++)
	{
		glomeruli[i].reset();
	}
	odors.clear();
	airs.clear();
}