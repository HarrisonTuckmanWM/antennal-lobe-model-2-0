#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Parameters.h"
#include "PN_Neuron.h"
#include "LN_Neuron.h"
using namespace std;



PN_Neuron::PN_Neuron()
{
	//---------------Euler Method Parameters--------------------------------------
	voltage = 0;																			//initialize voltage of Neuron
	refractory_period_internal = 0;															//initialize refractory period   
	excitation = 0;																			//initialize excitatory current input
	t_excitation = 0;																		//time of most recent excitation input
	fast_inhibition = 0;																	//initialize fast inhibition current input
	t_fast_inhibition = 0;																	//time of most recent fast inhibition input
	slow_inhibition = 0;																	//initialize slow inhibition current input
	t_slow_inhibition = 0;																	//time of most recent slow inhibition input																	//initialize leak current
	sk = 0;																					//initialize sk inhibition (PN neurons only)
	t_sk = 0;
	sk_times = {};
	input = 0;																				//initialize input from stimuli
	t_input = 0;
	//static default_random_engine generator;
	//generator.seed(1);
	normal_distribution<double> distribution(sk_mean, sk_std_dev);
	sk_strength = distribution(generator);
	if (sk_strength < 0) {
		sk_strength = 0;
	}
	printf("%f\n", sk_strength);

	//Neural Network Parameters
	PN_connections = {};																	//PN connections from this neuron to neurons in list
	LN_connections = {};																	//Ln connections from this neuron to neurons in list
}

PN_Neuron::~PN_Neuron()
{
}

void PN_Neuron::Integrate() {
	if (refractory_period_internal == 0) {
		double dV = -excitation * exp(-(t - t_excitation) / tau_excitation) / tau_excitation * (voltage - vpn)
			- fast_inhibition * exp(-(t - t_fast_inhibition) / tau_fast_inhibition) / tau_fast_inhibition * (voltage - vln)
			- slow_inhibition * exp(-(t - t_slow_inhibition) / tau_slow_inhibition) / tau_slow_inhibition * (voltage - vln)
			- leak * voltage - input * (voltage - vpn)*exp(-(t - t_input) / tau_input) / tau_input - calculateSk()/*sk * exp(-(t - t_sk) / tau_sk) / tau_sk*/ * (voltage - vln);					//Calculation of dV
		voltage += dV * timestep;															//recalculate voltage
		if (voltage >= 1.0) {																//condition for neuron firing
			voltage = 0;
			fprintf(fires, "1,");
			refractory_period_internal = refractory_counts;												//refractory period set for 2 ms 
			fire_to_PN();
			fire_to_LN();
			//sk = sk * exp(-(t - t_sk) / tau_sk) + sk_strength;
			//t_sk = t;
			sk_times.push_back(t);
			voltage_list.push_back(1);
		}
		else {
			voltage_list.push_back(voltage);
			fprintf(fires, "0,");
		}
	}
	else {
		voltage_list.push_back(voltage);
		refractory_period_internal -= 1;
		fprintf(fires, "0,");
	}
	//---------------File Data------------------------------------------
	excitation_list.push_back(excitation * exp(-(t - t_excitation) / tau_excitation) / tau_excitation);
	fast_inhibition_list.push_back(fast_inhibition * exp(-(t - t_fast_inhibition) / tau_fast_inhibition) / tau_fast_inhibition);
	slow_inhibition_list.push_back(slow_inhibition * exp(-(t - t_slow_inhibition) / tau_slow_inhibition) / tau_slow_inhibition);
	sk_list.push_back(sk * exp(-(t - t_sk) / tau_sk) / tau_sk);
	input_list.push_back(input * exp(-(t - t_input) / tau_input) / tau_input);
}

void PN_Neuron::add_PN_connection(PN_Neuron & neuron) {
	PN_connections.push_back(&neuron);
}

void PN_Neuron::add_LN_connection(LN_Neuron & neuron) {
	LN_connections.push_back(&neuron);
}

void PN_Neuron::receiving_excitation(double strength) {
	excitation = excitation * exp(-(t - t_excitation) / tau_excitation) + strength;
	t_excitation = t;
}

void PN_Neuron::receiving_fast_inhibition(double strength) {
	fast_inhibition = fast_inhibition * exp(-(t - t_fast_inhibition) / tau_fast_inhibition) + strength;
	t_fast_inhibition = t;
}

void PN_Neuron::receiving_slow_inhibition(double strength) {
	slow_inhibition = slow_inhibition * exp(-(t - t_slow_inhibition) / tau_slow_inhibition) + strength;
	t_slow_inhibition = t;
}

void PN_Neuron::receiving_input(double strength) {
	input = input * exp(-(t - t_input) / tau_input) + strength;
	t_input = t;
}

void PN_Neuron::fire_to_PN() {
	for (auto i : PN_connections) {
		i->receiving_excitation(PN_to_PN_strength);
	}
}

void PN_Neuron::fire_to_LN() {
	for (auto i : LN_connections) {
		i->receiving_excitation(PN_to_LN_strength);
	}
}

double PN_Neuron::get_voltage() {
	return voltage;
}

vector<double> PN_Neuron::get_voltage_list() {
	return voltage_list;
}

vector<double> PN_Neuron::get_excitation_list() {
	return excitation_list;
}

vector<double> PN_Neuron::get_slow_inhibition_list() {
	return slow_inhibition_list;
}

vector<double> PN_Neuron::get_fast_inhibition_list() {
	return fast_inhibition_list;
}

vector<double> PN_Neuron::get_sk_list() {
	return sk_list;
}

vector<double> PN_Neuron::get_input_list() {
	return input_list;
}

double PN_Neuron::calculateSk() {
	double to_return = 0;
	vector<int> to_delete = {};
	for (size_t i = 0; i < size(sk_times);i++) {
		if (t - sk_times[i] < sk_rise * 2) {
			to_return+=sk_strength * exp((sk_rise_tau*((t - sk_times[i]) - sk_rise))) / (1 + exp((sk_rise_tau*((t - sk_times[i]) - sk_rise))));
		}
		else {
			to_delete.push_back(i);
			sk = sk * exp(-(t - t_sk) / tau_sk) + sk_strength;
			t_sk = t;
		}
	}
	for (int i : to_delete) {
		sk_times.erase(sk_times.begin()+i);
	}
	to_return += sk * exp(-(t - t_sk) / tau_sk);
	return to_return/tau_sk;
}

void PN_Neuron::reset() {
	voltage = 0;																			//initialize voltage of Neuron
	refractory_period_internal = 0;															//initialize refractory period   
	excitation = 0;																			//initialize excitatory current input
	t_excitation = 0;																		//time of most recent excitation input
	fast_inhibition = 0;																	//initialize fast inhibition current input
	t_fast_inhibition = 0;																	//time of most recent fast inhibition input
	slow_inhibition = 0;																	//initialize slow inhibition current input
	t_slow_inhibition = 0;																	//time of most recent slow inhibition input																	//initialize leak current
	sk = 0;																					//initialize sk inhibition (PN neurons only)
	t_sk = 0;
	input = 0;																				//initialize input from stimuli
	t_input = 0;
	voltage_list.clear();
	excitation_list.clear();
	fast_inhibition_list.clear();
	slow_inhibition_list.clear();
	sk_list.clear();
	input_list.clear();
	sk_times.clear();
}