#pragma once
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Parameters.h"
#include "Glomerulus.h"
using namespace std;

class Odor_Pulse
{
public:
	Odor_Pulse(vector<int> send_to, double start, double duration);
	~Odor_Pulse();
	void update();
	double get_PN_signal();
	double get_LN_signal();
	vector<int> get_glomeruli();
	void reset();
private:
	double signal;
	double t_on;
	double t_off;
	double t_r;
	double new_max;
	bool started;
	bool type;
	vector<int> receivers;
};

