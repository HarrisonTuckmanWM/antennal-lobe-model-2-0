#include "Odor_Pulse.h"



Odor_Pulse::Odor_Pulse(vector<int> send_to,double start,double duration)
{
	t_on = start;
	t_off = start + duration;
	t_r = start + 2*t_rise;
	receivers = send_to;
	started = false;
	type = t_r < t_off;
	if (!type){
		new_max = max_odor_rate * exp((rise_tau * ((t_off - t_on) - t_rise))) / (1 + exp((rise_tau * ((t_off - t_on) - t_rise))));
	}
	signal = 0;
}


Odor_Pulse::~Odor_Pulse()
{
}

void Odor_Pulse::update() {
	if (!started) {
		if (t >= t_on - timestep) {
			started = true;
		}
	}
	else {
		if (type) {
			if (t < t_r) {
				signal = max_odor_rate * exp((rise_tau * ((t - t_on) - t_rise))) / (1 + exp((rise_tau * ((t - t_on) - t_rise))));
			}
			else if (t < t_off) {
				signal = max_odor_rate;
			}
			else {
				signal = max_odor_rate * exp(-(t - t_off) / odor_tau);
			}
		}
		else {
			if (t < t_off) {
				signal = max_odor_rate * exp((rise_tau * ((t - t_on) - t_rise))) / (1 + exp((rise_tau * ((t - t_on) - t_rise))));
			}
			else {
				signal=new_max * exp(-(t - t_off) / odor_tau);
			}
		}
	}
}

double Odor_Pulse::get_PN_signal() {
	return signal;
}

double Odor_Pulse::get_LN_signal() {
	if (started) {
		if (t < t_off) {
			return max_odor_rate;
		}
		else {
			return max_odor_rate * exp(-(t - t_off) / odor_tau);
		}
	}
	else {
		return signal;
	}
	
}

vector<int> Odor_Pulse::get_glomeruli() {
	return receivers;
}

void Odor_Pulse::reset() {
	started = false;
	signal = 0;
}

