#pragma once
#include <random>
#include <vector>
using namespace std;

extern const int mode;
extern const int trials;
extern int cur_brain;

//Global Fires
//extern FILE * fires;
extern FILE * fires;
//static default_random_engine generator;
static default_random_engine generator;

//timing

extern double t;
extern const double timestep;
extern const double endtime;

//Network Architecture

extern const int PN_Neurons;
extern const int LN_Neurons;
extern const int number_glomeruli;

//Connection Probabilities

extern const double PN_to_PN_prob;
extern const double PN_to_LN_prob;
extern const double LN_to_PN_prob;
extern const double LN_to_LN_prob;
extern double inter_LN_to_PN_prob;

//Neuron Parameters

extern const double leak;
extern const double vpn;
extern const double vln;
extern const unsigned int refractory_period;
extern const unsigned int refractory_counts;
extern const double tau_excitation;
extern const double tau_fast_inhibition;
extern const double tau_slow_inhibition;
extern const double tau_sk;
extern const double tau_input;
extern const double PN_to_LN_strength;
extern const double PN_to_PN_strength;
extern const double LN_to_LN_fast_strength;
extern const double LN_to_LN_slow_strength;
extern double LN_to_PN_fast_strength;
extern double LN_to_PN_slow_strength;
extern double sk_mean;
extern double sk_std_dev;
extern const double input_to_PN_strength;
extern const double input_to_LN_strength;

//SK Calculation Parameters
extern const double sk_rise; //half rise time
extern const double sk_rise_tau;

//Odor Parameters

extern const double t_rise;
extern const double rise_tau;
extern const double background_odor_rate; //units of ms^-1
extern double max_odor_rate;
extern const double odor_tau;

//Air Parameters

extern const double air_t_rise; //half rise time
extern const double air_rise_tau;
extern double max_air_rate;
extern const double air_tau;
