#pragma once
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Glomerulus.h"
#include "Parameters.h"
#include "Odor_Pulse.h"
#include "Air_Puff.h"
#include "Parameters.h"
using namespace std;

class Antennal_Lobe
{
public:
	Antennal_Lobe();
	~Antennal_Lobe();
	void interconnect();
	void update();
	void write_times(string dir);
	void write_voltage(string dir);
	void write_excitation(string dir);
	void write_slow_inhibition(string dir);
	void write_fast_inhibition(string dir);
	void write_sk(string dir);
	void write_input(string dir);
	void write_all(string dir);
	void add_odor(Odor_Pulse & odor);
	void clear_odors();
	void add_air(Air_Puff & air);
	void clear_airs();
	void reset();
	void hard_reset();
	//--------writeup------------------------
	vector<double> times = {};

private:
	vector<Glomerulus> glomeruli;
	vector<Odor_Pulse*> odors;
	vector<Air_Puff*> airs;
	
	

};

