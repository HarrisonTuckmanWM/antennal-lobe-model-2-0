#pragma once
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include "Parameters.h"
#include "Glomerulus.h"
using namespace std;

class Air_Puff
{
public:
	Air_Puff(double start, double duration);
	~Air_Puff();
	void update();
	double get_PN_signal();
	double get_LN_signal();
	void reset();
private:
	double signal;
	double t_on;
	double t_off;
	double t_r;
	double new_max;
	bool started;
	bool type;
};

