#include "Air_Puff.h"



Air_Puff::Air_Puff(double start, double duration)
{
	t_on = start;
	t_off = start + duration;
	t_r = start + 2 * air_t_rise;
	started = false;
	type = t_r < t_off;
	if (!type) {
		new_max = max_air_rate * exp((air_rise_tau * ((t_off - t_on) - air_t_rise))) / (1 + exp((air_rise_tau * ((t_off - t_on) - air_t_rise))));
	}
	signal = 0;
}


Air_Puff::~Air_Puff()
{
}

void Air_Puff::update() {
	if (!started) {
		if (t >= t_on - timestep) {
			started = true;
		}
	}
	else {
		if (type) {
			if (t < t_r) {
				signal = max_air_rate * exp((air_rise_tau * ((t - t_on) - air_t_rise))) / (1 + exp((air_rise_tau * ((t - t_on) - air_t_rise))));
			}
			else if (t < t_off) {
				signal = max_air_rate;
			}
			else {
				signal = max_air_rate * exp(-(t - t_off) / air_tau);
			}
		}
		else {
			if (t < t_off) {
				signal = max_air_rate * exp((air_rise_tau * ((t - t_on) - air_t_rise))) / (1 + exp((air_rise_tau * ((t - t_on) - air_t_rise))));
			}
			else {
				signal = new_max * exp(-(t - t_off) / air_tau);
			}
		}
	}
}

double Air_Puff::get_PN_signal() {
	//return signal;
	if (started) {
		if (t < t_off) {
			return max_air_rate;
		}
		else {
			return max_air_rate * exp(-(t - t_off) / air_tau);
		}
	}
	else {
		return signal;
	}
}

double Air_Puff::get_LN_signal() {
	/*if (started && t < t_off) {
		return max_air_rate;
	}
	else {
		return signal;
	}*/
	return signal;
}

void Air_Puff::reset() {
	started = false;
	signal = 0;
}

